# Platformio IO

Obsługa eXtrino XL w środowsku programistycznym **Platformio IO**

* [PlatformioIO](http://platformio.org/)
* [Visual Studio Code](https://code.visualstudio.com/)

## Dodanie konfiguracji płytki eXtrino XL w Platformio IO

Przed utworzeniem pierwszego projektu musimy dodać opis płytki eXtrino do konfiguracji **Platformio IO**. W tym celu plik z katalogu [boards/](boards/) wgrywamy do katalogu:
 * Linux/Mac: `~/.platformio/platforms/atmelavr/boards/`
 * Windows: Who knows? ;)

## Tworzenie projektu

Po utworzeniu projektu wgrywamy do głownego katalogu pliki z folderu [projekt/](projekt/)

## Ustawienia

 * Formatowanie kodu: **clang-format**
 * Kodowanie: **UTF-8**
 * Znak końca linii: **Unix LF**

## VSCode - Visual Studio Code [Microsoft]

### IntelliSense i <avr/io.h> a xmega128a3u
W celu poprawnego działania **IntelliSense** w **VSCode** upewnij się, że plik twojego projektu `.vscode/c_cpp_properties.json` w sekcji `defines` ma dokładnie tak samo określony rodzaj mikrokontrolera:

```
            "defines": [
                "__AVR_ATxmega128A3U__",
```

Więcej informacji pod linkiem: https://github.com/Microsoft/vscode-cpptools/issues/690

### avrdude i FLIP (FLIP2)
Upload pliku HEX odbywa się za pomocą oprogramowania FLIP (ang. FLexible In-system Programmer) firmy Atmel wbudowanego w bootloader XMEGA128A3U. Obsługiwany jest przez `avrdude` a uruchamiany poniszym poleceniem:

```avrdude -c flip2 -U application:w:.pioenvs/extrino/firmware.hex:i -p x128a3u ```

Patrz plik [upload.sh](upload.sh)

## Autorzy
* Paweł 'felixd' Wojciechowski - <http://www.konopnickiej.com>
